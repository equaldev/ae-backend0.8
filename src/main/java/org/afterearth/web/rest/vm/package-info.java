/**
 * View Models used by Spring MVC REST controllers.
 */
package org.afterearth.web.rest.vm;
