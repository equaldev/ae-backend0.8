package org.afterearth.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.afterearth.domain.AOI;
import org.afterearth.repository.UserRepository;
import org.afterearth.repository.AOIRepository;
import org.afterearth.security.AuthoritiesConstants;
import org.afterearth.security.SecurityUtils;
import org.afterearth.web.rest.util.HeaderUtil;
import org.afterearth.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AOI.
 */
@RestController
@RequestMapping("/api")
public class AOIResource {

    private final Logger log = LoggerFactory.getLogger(AOIResource.class);

    private static final String ENTITY_NAME = "aOI";

    @Inject
    private UserRepository userRepository;

    private final AOIRepository aOIRepository;

    public AOIResource(AOIRepository aOIRepository) {
        this.aOIRepository = aOIRepository;
    }

    /**
     * POST  /a-ois : Create a new aOI.
     *
     * @param aOI the aOI to create
     * @return the ResponseEntity with status 201 (Created) and with body the new aOI, or with status 400 (Bad Request) if the aOI has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/a-ois")
    @Timed
    public ResponseEntity<AOI> createAOI(@RequestBody AOI aOI) throws URISyntaxException {
        log.debug("REST request to save AOI : {}", aOI);
        if (aOI.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new aOI cannot already have an ID")).body(null);
        }

        if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {

                log.debug("No user passed in, using current user: {}",
                    SecurityUtils.getCurrentUserLogin());

            log.debug("from getCurrentUserLogin: {}",
                SecurityUtils.getCurrentUserLogin()
            );
            aOI.setUser(userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).get());

        }

        AOI result = aOIRepository.save(aOI);
        return ResponseEntity.created(new URI("/api/a-ois/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /a-ois : Updates an existing aOI.
     *
     * @param aOI the aOI to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated aOI,
     * or with status 400 (Bad Request) if the aOI is not valid,
     * or with status 500 (Internal Server Error) if the aOI couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/a-ois")
    @Timed
    public ResponseEntity<AOI> updateAOI(@RequestBody AOI aOI) throws URISyntaxException {
        log.debug("REST request to update AOI : {}", aOI);
        if (aOI.getId() == null) {
            return createAOI(aOI);
        }
        AOI result = aOIRepository.save(aOI);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, aOI.getId().toString()))
            .body(result);
    }

    /**
     * GET  /a-ois : get all the aOIS.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of aOIS in body
     */
    @GetMapping("/a-ois")
    @Timed
    public ResponseEntity<List<AOI>> getAllAOIS(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of AOIS");
//        Page<AOI> page = aOIRepository.findByUserIsCurrentUser(pageable);
        Page<AOI> page;

        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {

            page = aOIRepository.findAll(pageable);

        } else {

            page = aOIRepository.findByUserIsCurrentUser(pageable);

        }

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/a-ois");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /a-ois/:id : get the "id" aOI.
     *
     * @param id the id of the aOI to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the aOI, or with status 404 (Not Found)
     */
    @GetMapping("/a-ois/{id}")
    @Timed
    public ResponseEntity<AOI> getAOI(@PathVariable Long id) {
        log.debug("REST request to get AOI : {}", id);
        AOI aOI = aOIRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(aOI));
    }

    /**
     * DELETE  /a-ois/:id : delete the "id" aOI.
     *
     * @param id the id of the aOI to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/a-ois/{id}")
    @Timed
    public ResponseEntity<Void> deleteAOI(@PathVariable Long id) {
        log.debug("REST request to delete AOI : {}", id);
        aOIRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
