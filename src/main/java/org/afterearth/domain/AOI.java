package org.afterearth.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A AOI.
 */
@Entity
@Table(name = "aoi")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AOI implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "jhi_after")
    private LocalDate after;

    @Column(name = "jhi_until")
    private LocalDate until;

    @Column(name = "title")
    private String title;

    @Column(name = "lat")
    private String lat;

    @Column(name = "lng")
    private String lng;

    @ManyToOne
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getAfter() {
        return after;
    }

    public AOI after(LocalDate after) {
        this.after = after;
        return this;
    }

    public void setAfter(LocalDate after) {
        this.after = after;
    }

    public LocalDate getUntil() {
        return until;
    }

    public AOI until(LocalDate until) {
        this.until = until;
        return this;
    }

    public void setUntil(LocalDate until) {
        this.until = until;
    }

    public String getTitle() {
        return title;
    }

    public AOI title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLat() {
        return lat;
    }

    public AOI lat(String lat) {
        this.lat = lat;
        return this;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public AOI lng(String lng) {
        this.lng = lng;
        return this;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public User getUser() {
        return user;
    }

    public AOI user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AOI aOI = (AOI) o;
        if (aOI.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), aOI.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AOI{" +
            "id=" + getId() +
            ", after='" + getAfter() + "'" +
            ", until='" + getUntil() + "'" +
            ", title='" + getTitle() + "'" +
            ", lat='" + getLat() + "'" +
            ", lng='" + getLng() + "'" +
            "}";
    }
}
