package org.afterearth.repository;

import org.afterearth.domain.AOI;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data JPA repository for the AOI entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AOIRepository extends JpaRepository<AOI,Long> {

    @Query("select aoi from AOI aoi where aoi.user.login = ?#{principal.username}")
    Page<AOI> findByUserIsCurrentUser(Pageable pageable);

//    @Query("select aoi from AOI aoi where aoi.user.login = ?#{principal.username} order by aoi.date desc")
//    Page<AOI> findAllByOrderByDateDesc(Pageable pageable);
}
