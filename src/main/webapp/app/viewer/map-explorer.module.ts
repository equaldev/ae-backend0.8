import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AfterearthSharedModule } from '../shared';

import { PREVIEW_ROUTE, MapExplorerComponent } from './index';

@NgModule({
    imports: [
        AfterearthSharedModule,
        RouterModule.forRoot([ PREVIEW_ROUTE ], { useHash: true })
    ],
    declarations: [
        MapExplorerComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AfterearthExplorerModule {}
