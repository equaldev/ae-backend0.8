import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';



declare const L: any; // --> Works

// import 'leaflet.vectorgrid';
import 'leaflet';
import 'leaflet-control-geocoder';

@Component({
    selector: 'ae-explorer',
    templateUrl: './map-explorer.component.html',
    styleUrls: [
        './Control.Geocoder.css'
    ],
  encapsulation: ViewEncapsulation.None,

})
export class MapExplorerComponent implements OnInit {
    map: L.Map = null;
    
    @ViewChild('previewMap') mapContainer;

    
    constructor() {
    }

    ngOnInit() {

        const urlLayer3 = `https://api.mapbox.com/styles/v1/mapbox/satellite-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZXF1aWxsaWJyaXVtIiwiYSI6IjRiM2YwNGI3OTgxODRjNjhhZWJlMTVmNDQ5YzIwZWI1In0.la27BQntMyeBoN-Ea4m45w`;
        const urlLayer4 = `https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png`;
        const urlLayer5 = `https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png'`; 
    
        
       const satellite = L.tileLayer(urlLayer3,{attribution: '© MapBox. Mapbox.com'});
       const osm = L.tileLayer(urlLayer4,{attribution: '© OpenStreetMap contributors. OpenStreetMap.org'});
       const topo = L.tileLayer(urlLayer5,{attribution: '© OpenStreetMap contributors. OpenTopoMap.org'});
        const baseMaps = {
            "Satellite": satellite,
            "OSM": osm,
            "Topographic": topo
        };

        this.map = L.map(this.mapContainer.nativeElement, {
            center: [39.73, -104.99],
            zoom: 10,
            layers: [satellite, osm, topo]
        });
        L.Control.geocoder().addTo(this.map);
        L.control.layers(baseMaps).addTo(this.map);
        
        
    }

   

    
}
