import { Route } from '@angular/router';

import { UserRouteAccessService } from '../shared';
import { MapExplorerComponent } from './index';

export const PREVIEW_ROUTE: Route = {
    path: 'explorer',
    component: MapExplorerComponent,
    data: {
        authorities: [],
        pageTitle: 'home.title'
    }
};
