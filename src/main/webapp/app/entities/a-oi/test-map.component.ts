import { Component, OnInit, ViewEncapsulation, ViewChild, Input, AfterViewInit } from '@angular/core';
//import * as L from 'leaflet';
declare const L: any; // --> Works

// import 'leaflet.vectorgrid';
import 'leaflet';
import 'leaflet-draw';
import 'leaflet.fullscreen';
import 'leaflet-easyprint';
import 'leaflet.browser.print/dist/leaflet.browser.print.min.js';
import 'leaflet-draw/dist/leaflet.draw.css';
import 'togeojson/togeojson.js';
import 'leaflet-filelayer/src/leaflet.filelayer.js';
import { AOI } from './aoi.model';

@Component({
  selector: 'ae-map-layers',
  template: '<div id="map" #map style="height:600px;"><div id="map-ui"></div></div><br>  <a class="btn btn-success" id="export">Export Features</a>',
    styleUrls: ['./map.service.css',
    './leaflet.draw.css',
    './fs/Control.FullScreen.css',

  ], encapsulation: ViewEncapsulation.None,

})
export class MapLayersComponent implements OnInit, AfterViewInit {

  map: L.Map = null;

  @ViewChild('map') mapContainer;
  @Input() aoi: AOI;
  until: Date;
  after: Date;
 // drawnItems = new L.FeatureGroup();
  
  constructor() {
  }

  ngOnInit() {

    // Init map
    this.map = L.map(this.mapContainer.nativeElement)
      .setView([this.aoi.lat, this.aoi.lng], 13);

    // Create a FeatureGroup for storage and export
    let drawnItems =  new L.FeatureGroup();
    this.map.addLayer(drawnItems);

    //Initiate boundary to reduce map tile requests
    let mBounds = this.map.getBounds();
    this.map.setMaxBounds(mBounds);

    // Preparing the AOI Date Values
    this.after = this.aoi.after.toISOString();
    this.until = this.aoi.until.toISOString();
    
    // Add Scale Bar to bottom of map
    L.control.scale().addTo(this.map);

     // Fullscreen Options
     L.control.fullscreen({
      position: 'topleft', // change the position of the button can be topleft, topright, bottomright or bottomleft, defaut topleft 
      title: 'Open Fullscreen', // change the title of the button, default Full Screen 
      titleCancel: 'Exit Fullscreen', // change the title of the button when fullscreen is on, default Exit Full Screen 
      content: null, // change the content of the button, can be HTML, default null 
      forceSeparateButton: true, // force seperate button to detach from zoom buttons, default false 
      forcePseudoFullscreen: false, // force use of pseudo full screen even if full screen API is available, default false 
      fullscreenElement: false // Dom element to render in full screen, false by default, fallback to map._container 
    }).addTo(this.map);

    /*----------------------------------
              TOOLS/UTILITIES
    ------------------------------------ */

    // 
    let printer = L.easyPrint({
      sizeModes: ['Current', 'A4Landscape', 'A4Portrait'],
      filename: 'ae-export',
      exportOnly: true,
    }).addTo(this.map);

      /*----------------------------------
              IMPORT/EXPORT
    ------------------------------------ */

    // Export FeatureGroup function.
    let tmpAOI = this.aoi;
    document.getElementById('export').onclick = function (e) {
        // Extract GeoJson from featureGroup
        let data = drawnItems.toGeoJSON();
        
        // Stringify the GeoJson
        let convertedData = 'text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(data));
        let fileName = 'ae-features_'+ tmpAOI.id +'_'+ tmpAOI.user.login +'.geojson';
        // Create export
        document.getElementById('export').setAttribute('href', 'data:' + convertedData);
        document.getElementById('export').setAttribute('download', fileName);
      
        // this.printer.printMap('CurrentSize', 'ae-export');
    }

    // Import FeatureGroups as GeoJSON or KML
   L.Control.FileLayerLoad.LABEL = '<i class="fa fa-file-text" aria-hidden="true"></i>';   
   let fL =  L.Control.fileLayerLoad({
      layer: L.geoJson,
      // See http://leafletjs.com/reference.html#geojson-options
      layerOptions: {style: {color:'red', fillOpacity: 0.2}},
      // Add to map after loading (default: true) ?
      addToMap: true,
      // File size limit in kb (default: 1024) ?
      fileSizeLimit: 1024,
      // Restrict accepted file formats (default: .geojson, .json, .kml, and .gpx) ?
      formats: [
          '.geojson',
          '.kml'
      ]
    }).addTo(this.map);

    // let control = L.Control.fileLayerLoad();
    // control.loader.on('data:loaded', function (event) {
    //     // event.layer gives you access to the layers you just uploaded!

    //     // Add to map layer switcher
    //     drawnItems.addLayer(event.layer);
    //   });

  // let control = L.Control.FileLayerLoad({
  //     fitBounds: true
  // });

  // control.addTo(this.map);

   // Export to PDF functions: will tackle this soon.

    // let toPDF = L.browserPrint({
    //   title: 'Export as PDF',
    //   closePopupsOnPrint: false,
    //   printModes:["Portrait", "Landscape", "Custom"],
    //   printModesNames: { 
    //     Portrait: "Portrait", 
    //     Landscape: "Landscape", 
    //     Custom: "Manually select area" }
    // }).addTo(this.map);

    // this.map.on("browser-print-start", function(e:any){
    //   /*on print start we already have a print map and we can create new control and add it to the print map to be able to print custom information */
    //                      console.log(e);
    //   //drawnItems.addTo(e.printMap);
    //   let tmp =  new L.FeatureGroup()
    //   tmp.addLayer(drawnItems).addTo(e.printMap);
  
    // });


    // Assign Draw Control to above drawnItems and set Draw parameters
    let drawControl = new L.Control.Draw({
      edit: {
        featureGroup: drawnItems
      },
      draw: {
        polyline: {
          shapeOptions: {
            color: 'red',
            showLength: true,
            showMeasurements: true
          },
          metric: true,
          showLength: true,
          showMeasurements: true
        },
        rectangle: {
          shapeOptions: {
            color: 'green',
            showArea: true,
            showMeasurements: true
          },
          metric: true,
          showArea: true,
          showMeasurements: true
        },
        polygon: {
          shapeOptions: {
            color: 'purple',
            showArea: true,
            showMeasurements: true
          },
          showArea: true,
          showMeasurements: true
        }
      }
    });

    // Handler for creation of Features. 
    this.map.on('draw:created', function (e: any) {
      let type = e.layerType,
        layer = e.layer;

      if (type === 'marker') {

        let name = prompt("enter a title");
        let desc = prompt("enter a description");
        let currentTime = new Date();

        console.log(layer);
        layer.bindPopup('<span><b>Name:   </b></span>' + name +
          '<br/><br/><span><b>Description:   </b></span>' + desc +
          '<br/><br/><span><b>Latitude:   </b></span>' + layer._latlng.lat +
          '<br/><br/><span><b>Longitude:   </b></span>' + layer._latlng.lng +
          '<br/><br/><span><b>Created:   </b></span>' + currentTime +
          '<br/>');
      }

      if (type === 'polyline') {

        let name = prompt("enter a title");
        let desc = prompt("enter a description");
        let currentTime = new Date();

        console.log(layer);
        layer.bindPopup('<span><b>Name:   </b></span>' + name +
          '<br/><br/><span><b>Description:   </b></span>' + desc +
          // Find way to add total length of line to popup
          // '<br/><br/><span><b>Latitude:   </b></span>'+ layer._measurementLayer._layers[lengthIndex]._measurement +
          '<br/><br/><span><b>Created:   </b></span>' + currentTime +
          '<br/>');
        // let _length = L.GeometryUtil.length([layer._latlngs["0"], layer._latlngs[layer._latlngs.length - 1]]);
        // console.log(_length);

      }

      if (type === 'polygon') {

        let name = prompt("enter a title");
        let desc = prompt("enter a description");
        let currentTime = new Date();

        let seeArea = L.GeometryUtil.geodesicArea(layer.getLatLngs()[0]);
        //let seeaArea = L.GeometryUtil.geodesicArea(layer._latlngs[0]);

        layer.bindPopup('<span><b>Name:   </b></span>' + name +
          '<br/><br/><span><b>Description:   </b></span>' + desc +
          '<br/><br/><span><b>Area:   </b></span>' + (seeArea / 100000).toFixed(2) + " km<sup>2</sup>" +
          '<br/><br/><span><b>Created:   </b></span>' + currentTime +
          '<br/>');
      }
      drawnItems.addLayer(layer);
    });

    // Add previously declared options to map
    this.map.addControl(drawControl);

  
    /*----------------------------------
              TILE LAYERS
    ------------------------------------ */

    // These tiles be for testing.
    const urlLayer3 = `https://api.mapbox.com/styles/v1/mapbox/satellite-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZXF1aWxsaWJyaXVtIiwiYSI6IjRiM2YwNGI3OTgxODRjNjhhZWJlMTVmNDQ5YzIwZWI1In0.la27BQntMyeBoN-Ea4m45w`;
    const urlLayer4 = `https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png`;
    const urlLayer5 = `https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png'`; 

    
    this.addLayer(L.tileLayer(urlLayer3,{attribution: '© MapBox. Mapbox.com'}).addTo(this.map), 'HiRes', 1);
    this.addLayer(L.tileLayer(urlLayer4,{attribution: '© OpenStreetMap contributors. OpenStreetMap.org'}).addTo(this.map), 'OSM', 1);
    this.addLayer(L.tileLayer(urlLayer5,{attribution: '© OpenStreetMap contributors. OpenTopoMap.org'}).addTo(this.map), 'Topographic', 1);
    
 
  }

  addLayer(layer: any, name: any, zIndex) {

    layer.setZIndex(zIndex).addTo(this.map);

    const aMap = this.map;

    const link = document.createElement('a');

    link.href = '#';

    link.className = 'active';

    link.innerHTML = name;

    link.onclick = function (e: any) {
      e.preventDefault();
      e.stopPropagation();

      if (aMap.hasLayer(layer)) {
        aMap.removeLayer(layer);
        this.className = '';
      } else {
        aMap.addLayer(layer);
        this.className = 'active';
      }
    };

    const layers = document.getElementById('map-ui');

    layers.appendChild(link);
  }

  ngAfterViewInit() {
    //Fixes bug with blank map tiles
    this.map.invalidateSize();

    // this.elementRef.nativeElement.querySelector(".saveF")
    // .addEventListener('click', (e)=>
    // {
    //   // get id from attribute
    //   //var merchId = e.target.getAttribute("data-merchId");
    //   console.log(e);
    //  // this.saveFeature();
    // });
  }

  exportFeatures() {
        
    
      }
}
