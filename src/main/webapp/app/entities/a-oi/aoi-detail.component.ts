import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { AOI } from './aoi.model';
import { AOIService } from './aoi.service';

@Component({
    selector: 'jhi-aoi-detail',
    templateUrl: './aoi-detail.component.html'
})
export class AOIDetailComponent implements OnInit, OnDestroy {

    aOI: AOI;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private aOIService: AOIService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAOIS();
    }

    load(id) {
        this.aOIService.find(id).subscribe((aOI) => {
            this.aOI = aOI;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAOIS() {
        this.eventSubscriber = this.eventManager.subscribe(
            'aOIListModification',
            (response) => this.load(this.aOI.id)
        );
    }
}
