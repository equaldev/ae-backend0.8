import { BaseEntity, User } from './../../shared';

export class AOI implements BaseEntity {
    constructor(
        public id?: number,
        public after?: any,
        public until?: any,
        public title?: string,
        public lat?: number,
        public lng?: number,
        public user?: User,
    ) {
    }
}
