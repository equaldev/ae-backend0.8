import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AfterearthSharedModule } from '../../shared';
import { AfterearthAdminModule } from '../../admin/admin.module';
import {
    AOIService,
    AOIPopupService,
    AOIComponent,
    MapLayersComponent,
    AOIDetailComponent,
    AOIDialogComponent,
    AOIPopupComponent,
    AOIDeletePopupComponent,
    AOIDeleteDialogComponent,
    aOIRoute,
    aOIPopupRoute,
} from './';

const ENTITY_STATES = [
    ...aOIRoute,
    ...aOIPopupRoute,
];

@NgModule({
    imports: [
        AfterearthSharedModule,
        AfterearthAdminModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AOIComponent,
        AOIDetailComponent,
        AOIDialogComponent,
        MapLayersComponent,
        AOIDeleteDialogComponent,
        AOIPopupComponent,
        AOIDeletePopupComponent,
    ],
    entryComponents: [
        AOIComponent,
        AOIDialogComponent,
        AOIPopupComponent,
        AOIDeleteDialogComponent,
        AOIDeletePopupComponent,
    ],
    providers: [
        AOIService,
        AOIPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AfterearthAOIModule {}
