package org.afterearth.web.rest;

import org.afterearth.AfterearthApp;

import org.afterearth.domain.AOI;
import org.afterearth.repository.AOIRepository;
import org.afterearth.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AOIResource REST controller.
 *
 * @see AOIResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AfterearthApp.class)
public class AOIResourceIntTest {

    private static final LocalDate DEFAULT_AFTER = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_AFTER = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UNTIL = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UNTIL = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_LAT = "AAAAAAAAAA";
    private static final String UPDATED_LAT = "BBBBBBBBBB";

    private static final String DEFAULT_LNG = "AAAAAAAAAA";
    private static final String UPDATED_LNG = "BBBBBBBBBB";

    @Autowired
    private AOIRepository aOIRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAOIMockMvc;

    private AOI aOI;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AOIResource aOIResource = new AOIResource(aOIRepository);
        this.restAOIMockMvc = MockMvcBuilders.standaloneSetup(aOIResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AOI createEntity(EntityManager em) {
        AOI aOI = new AOI()
            .after(DEFAULT_AFTER)
            .until(DEFAULT_UNTIL)
            .title(DEFAULT_TITLE)
            .lat(DEFAULT_LAT)
            .lng(DEFAULT_LNG);
        return aOI;
    }

    @Before
    public void initTest() {
        aOI = createEntity(em);
    }

    @Test
    @Transactional
    public void createAOI() throws Exception {
        int databaseSizeBeforeCreate = aOIRepository.findAll().size();

        // Create the AOI
        restAOIMockMvc.perform(post("/api/a-ois")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aOI)))
            .andExpect(status().isCreated());

        // Validate the AOI in the database
        List<AOI> aOIList = aOIRepository.findAll();
        assertThat(aOIList).hasSize(databaseSizeBeforeCreate + 1);
        AOI testAOI = aOIList.get(aOIList.size() - 1);
        assertThat(testAOI.getAfter()).isEqualTo(DEFAULT_AFTER);
        assertThat(testAOI.getUntil()).isEqualTo(DEFAULT_UNTIL);
        assertThat(testAOI.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testAOI.getLat()).isEqualTo(DEFAULT_LAT);
        assertThat(testAOI.getLng()).isEqualTo(DEFAULT_LNG);
    }

    @Test
    @Transactional
    public void createAOIWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = aOIRepository.findAll().size();

        // Create the AOI with an existing ID
        aOI.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAOIMockMvc.perform(post("/api/a-ois")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aOI)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<AOI> aOIList = aOIRepository.findAll();
        assertThat(aOIList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAOIS() throws Exception {
        // Initialize the database
        aOIRepository.saveAndFlush(aOI);

        // Get all the aOIList
        restAOIMockMvc.perform(get("/api/a-ois?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aOI.getId().intValue())))
            .andExpect(jsonPath("$.[*].after").value(hasItem(DEFAULT_AFTER.toString())))
            .andExpect(jsonPath("$.[*].until").value(hasItem(DEFAULT_UNTIL.toString())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].lat").value(hasItem(DEFAULT_LAT.toString())))
            .andExpect(jsonPath("$.[*].lng").value(hasItem(DEFAULT_LNG.toString())));
    }

    @Test
    @Transactional
    public void getAOI() throws Exception {
        // Initialize the database
        aOIRepository.saveAndFlush(aOI);

        // Get the aOI
        restAOIMockMvc.perform(get("/api/a-ois/{id}", aOI.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(aOI.getId().intValue()))
            .andExpect(jsonPath("$.after").value(DEFAULT_AFTER.toString()))
            .andExpect(jsonPath("$.until").value(DEFAULT_UNTIL.toString()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.lat").value(DEFAULT_LAT.toString()))
            .andExpect(jsonPath("$.lng").value(DEFAULT_LNG.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAOI() throws Exception {
        // Get the aOI
        restAOIMockMvc.perform(get("/api/a-ois/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAOI() throws Exception {
        // Initialize the database
        aOIRepository.saveAndFlush(aOI);
        int databaseSizeBeforeUpdate = aOIRepository.findAll().size();

        // Update the aOI
        AOI updatedAOI = aOIRepository.findOne(aOI.getId());
        updatedAOI
            .after(UPDATED_AFTER)
            .until(UPDATED_UNTIL)
            .title(UPDATED_TITLE)
            .lat(UPDATED_LAT)
            .lng(UPDATED_LNG);

        restAOIMockMvc.perform(put("/api/a-ois")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAOI)))
            .andExpect(status().isOk());

        // Validate the AOI in the database
        List<AOI> aOIList = aOIRepository.findAll();
        assertThat(aOIList).hasSize(databaseSizeBeforeUpdate);
        AOI testAOI = aOIList.get(aOIList.size() - 1);
        assertThat(testAOI.getAfter()).isEqualTo(UPDATED_AFTER);
        assertThat(testAOI.getUntil()).isEqualTo(UPDATED_UNTIL);
        assertThat(testAOI.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testAOI.getLat()).isEqualTo(UPDATED_LAT);
        assertThat(testAOI.getLng()).isEqualTo(UPDATED_LNG);
    }

    @Test
    @Transactional
    public void updateNonExistingAOI() throws Exception {
        int databaseSizeBeforeUpdate = aOIRepository.findAll().size();

        // Create the AOI

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAOIMockMvc.perform(put("/api/a-ois")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aOI)))
            .andExpect(status().isCreated());

        // Validate the AOI in the database
        List<AOI> aOIList = aOIRepository.findAll();
        assertThat(aOIList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAOI() throws Exception {
        // Initialize the database
        aOIRepository.saveAndFlush(aOI);
        int databaseSizeBeforeDelete = aOIRepository.findAll().size();

        // Get the aOI
        restAOIMockMvc.perform(delete("/api/a-ois/{id}", aOI.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AOI> aOIList = aOIRepository.findAll();
        assertThat(aOIList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AOI.class);
        AOI aOI1 = new AOI();
        aOI1.setId(1L);
        AOI aOI2 = new AOI();
        aOI2.setId(aOI1.getId());
        assertThat(aOI1).isEqualTo(aOI2);
        aOI2.setId(2L);
        assertThat(aOI1).isNotEqualTo(aOI2);
        aOI1.setId(null);
        assertThat(aOI1).isNotEqualTo(aOI2);
    }
}
