/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { AfterearthTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { AOIDetailComponent } from '../../../../../../main/webapp/app/entities/a-oi/aoi-detail.component';
import { AOIService } from '../../../../../../main/webapp/app/entities/a-oi/aoi.service';
import { AOI } from '../../../../../../main/webapp/app/entities/a-oi/aoi.model';

describe('Component Tests', () => {

    describe('AOI Management Detail Component', () => {
        let comp: AOIDetailComponent;
        let fixture: ComponentFixture<AOIDetailComponent>;
        let service: AOIService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [AfterearthTestModule],
                declarations: [AOIDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    AOIService,
                    JhiEventManager
                ]
            }).overrideTemplate(AOIDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AOIDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AOIService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new AOI(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.aOI).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
